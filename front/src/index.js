import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from "react-router-dom";
import App from "./App";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import usersReducer from "./store/reducers/usersReducer";
import history from "./history";
import postsReducer from "./store/reducers/postsReducer";
import commentsReducer from "./store/reducers/commentsReducer";

const saveToLocalStorage = state => {
    try {
        const serializedSate = JSON.stringify(state);
        localStorage.setItem('forumState', serializedSate);
    } catch (err) {
        console.log('Could not save state');
    }
};

const loadFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('forumState');
        if(serializedState === null) {
            return undefined;
        }

        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
}

const rootReducer = combineReducers({
    users: usersReducer,
    posts: postsReducer,
    comments: commentsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunk)));

store.subscribe(() => {
    saveToLocalStorage({
        users: store.getState().users
    });
})

const app = (
    <Provider store={store}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>
)

ReactDOM.render(app,document.getElementById('root'));