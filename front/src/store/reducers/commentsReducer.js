import {CREATE_COMMENT_SUCCESS, FETCH_COMMENTS_SUCCESS} from "../actions/commentsActions";

const initialState = {
    comments: [],
}

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_COMMENT_SUCCESS:
            return {...state, comments: [action.payload, ...state.comments]};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.payload};
        default:
            return state;
    }
};

export default commentsReducer;