import axiosApi from "../../axiosApi";

export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const createCommentSuccess = payload => ({type: CREATE_COMMENT_SUCCESS, payload});
export const createComment = (comment, user) => {
    return async dispatch => {
        try {
            const commentResponse = await axiosApi.post('/comments', comment, {headers: {Authorization: user.token}});
            dispatch(createCommentSuccess(commentResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const fetchCommentsSuccess = payload => ({type: FETCH_COMMENTS_SUCCESS, payload});
export const fetchComments = postId => {
    return async dispatch => {
        try {
            const commentsResponse = await axiosApi.get('/comments', {params: {post: postId}});
            dispatch(fetchCommentsSuccess(commentsResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}