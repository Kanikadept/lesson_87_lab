import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

//====================================================LOGOUT
export const LOGOUT_USER = 'LOGOUT_USER';
export const logoutUser = () => ({type: LOGOUT_USER})
//====================================================LOGIN USER
export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const loginUserRequest = () => ({type: LOGIN_USER_REQUEST})

export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const loginUserSuccess = payload => ({type: LOGIN_USER_SUCCESS, payload});
export const loginUser = payload => {
    return async dispatch => {
        try {
            dispatch(loginUserRequest());
            const userResponse = await axiosApi.post('/users/sessions', payload);
            dispatch(loginUserSuccess(userResponse.data.updatedUser));
            dispatch(historyPush('/'));
        } catch (err) {
            if(err.response && err.response.data) {
                dispatch(loginUserFailure(err.response.data));
            } else {
                dispatch(loginUserFailure({global: 'No internet'}));
            }
        }
    }
}
//=========================================================REGISTER USER
export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST';
export const registerUserRequest = () => ({type: REGISTER_USER_REQUEST})

export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';
export const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const registerUserSuccess = payload => ({type: REGISTER_USER_SUCCESS, payload});
export const registerUser = payload => {
    return async dispatch => {
        try {
            dispatch(registerUserRequest());
            const userResponse = await axiosApi.post('/users', payload);
            dispatch(registerUserSuccess(userResponse.data));
            dispatch(historyPush('/'));
        } catch (err) {
            if(err.response && err.response.data) {
                dispatch(registerUserFailure(err.response.data));
            } else {
                dispatch(registerUserFailure({global: 'No internet'}));
            }
        }
    }
}