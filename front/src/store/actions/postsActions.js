import axiosApi from "../../axiosApi";

export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const createPostSuccess = payload => ({type: CREATE_POST_SUCCESS, payload});
export const createPost = (payload, token) => {
    return async dispatch => {
        try {
            const postResponse = await axiosApi.post('/posts', payload, {headers: {Authorization: token}});
            dispatch(createPostSuccess(postResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}


export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const fetchPostsSuccess = (payload) => ({type: FETCH_POSTS_SUCCESS, payload});
export const fetchPosts = () => {
    return async dispatch => {
        try {
            const postsResponse = await axiosApi.get('/posts');
            dispatch(fetchPostsSuccess(postsResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}

export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const fetchPostSuccess = payload => ({type: FETCH_POST_SUCCESS, payload});
export const fetchPost = postId => {
    return async dispatch => {
        try {
            const postResponse = await axiosApi.get('/posts/' + postId);
            dispatch(fetchPostSuccess(postResponse.data));
        } catch (err) {
            console.log(err);
        }
    }
}