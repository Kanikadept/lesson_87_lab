import React from 'react';
import {NavLink} from "react-router-dom";

const AnonymousMenu = () => {
    return (
        <>
            <NavLink to="/register"><button className="header__btn__sign">Sign up</button></NavLink>
            <NavLink to="/login"><button className="header__btn__log">Log in</button></NavLink>
        </>
    );
};

export default AnonymousMenu;