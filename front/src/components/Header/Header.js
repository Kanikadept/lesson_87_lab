import React from 'react';
import UserMenu from "./Menu/UserMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";
import {useSelector} from "react-redux";
import './Header.css';

const Header = () => {

    const user = useSelector(state => state.users.user);

    return (
        <header className="header">
            <h3 className="header__tite">Forum</h3>
            <div className="header__btns">
                {user ? (
                    <UserMenu user={user}/>
                ) : (
                    <AnonymousMenu />
                )}
            </div>
        </header>
    );
};

export default Header;