import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchComments} from "../../store/actions/commentsActions";
import './Comments.css';
import {NavLink} from "react-router-dom";

const Comments = ({postId}) => {

    const dispatch = useDispatch();
    const comments = useSelector(state => state.comments.comments);

    useEffect(() => {
        dispatch(fetchComments(postId));
    }, [dispatch,postId])

    return (
        <div className="comments">
            <NavLink to={'/commentForm/'+ postId}>Add comment</NavLink>
            {comments.map(comment => {
                return <div key={comment._id} className="comment">
                    <span>{comment.text}</span>
                </div>
            })}
        </div>
    );
};

export default Comments;