import React, {useEffect, useState} from 'react';
import './CommentForm.css';
import {useDispatch, useSelector} from "react-redux";
import {createComment} from "../../../store/actions/commentsActions";

const CommentForm = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        if (!user) {
            props.history.push('/');
        }
    }, [user, props.history])

    const [comment, setComment] = useState({
        text: '',
    });

    const handleSubmit = event => {
        event.preventDefault();
        comment.post = props.match.params.id;
        dispatch(createComment(comment, user));
        props.history.push('/post/' + props.match.params.id);
    }

    return (
        <form onSubmit={handleSubmit} className="comment-form">
            <div className="comment-form__row">
                <label>Text</label>
                <input onChange={(e) => setComment(prev => ({...prev, text: e.target.value}))} type="text"/>
            </div>
            <button>Add</button>
        </form>
    );
};

export default CommentForm;