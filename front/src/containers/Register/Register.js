import React, {useState} from 'react';
import './Register.css';
import {useDispatch} from "react-redux";
import {registerUser} from "../../store/actions/usersActions";

const Register = () => {

    const dispatch = useDispatch();

    const [cred, setCred] = useState({
        username: '',
        password: '',
    });

    const handleChange = (event) => {
        const {name, value} = event.target;
        setCred(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const handleSubmit = event => {
        event.preventDefault();
        dispatch(registerUser({...cred}));
    }
    return (
        <form className="register" onSubmit={handleSubmit}>
            <span><i>Sign up</i></span>
            <div className="register__row">
                <label><b>Username:</b></label>
                <input type="text" name="username" value={cred.username} onChange={handleChange}/>
            </div>
            <div className="register__row">
                <label><b>Password:</b></label>
                <input type="password" name="password" value={cred.password} onChange={handleChange}/>
            </div>
            <div className="register__row">
                <button>Sign up</button>
            </div>
        </form>
    );
};

export default Register;