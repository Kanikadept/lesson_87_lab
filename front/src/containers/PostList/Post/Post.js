import React from 'react';
import './Post.css';
import {NavLink} from "react-router-dom";

const Post = ({id, title, author, image, date}) => {
    return (
        <div className="post">
            <div className="post__image-wrapper">
                <img className="post__image" src={'http://localhost:8000/' + image} alt=""/>
            </div>
            <div className="post__info">
                <span>{date} by {author}</span>
                <NavLink to={'post/' + id}>{title}</NavLink>
            </div>
        </div>
    );
};

export default Post;