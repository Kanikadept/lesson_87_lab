import React, {useEffect} from 'react';
import './PostList.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../../store/actions/postsActions";
import Post from "./Post/Post";

const PostList = () => {

    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    return (
        <div className="post-list">
            {posts.map(post => {
                return <Post key={post._id} title={post.title}
                             id={post._id}
                             author={post.user.username}
                             desc={post.description}
                             image={post.image} date={post.date}/>
            })}
        </div>
    );
};

export default PostList;