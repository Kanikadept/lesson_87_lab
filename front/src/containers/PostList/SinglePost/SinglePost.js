import React, {useEffect} from 'react';
import './SinglePost.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchPost} from "../../../store/actions/postsActions";
import Comments from "../../Comments/Comments";

const SinglePost = props => {

    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.post);

    useEffect(() => {
        dispatch(fetchPost(props.match.params.id));
    }, [dispatch, props.match.params.id])



    return (post &&
        <>
            <div className="single-post">
                <div className="single-post__image-wrapper">
                    <img className="single-post__image" src={'http://localhost:8000/' + post.image} alt=""/>
                </div>
                <span>Author: {post.user.username}</span>
                <span>Title: {post.title}</span>
                <span>Description: {post.description}</span>

            </div>
            <b>Comments</b>
            <Comments postId={post._id}/>
        </>
    );
};

export default SinglePost;