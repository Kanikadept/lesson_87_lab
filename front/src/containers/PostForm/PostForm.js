import React, {useEffect, useState} from 'react';
import './PostForm.css';
import FileInput from "./FileInput/FileInput";
import {useDispatch, useSelector} from "react-redux";
import {createPost} from "../../store/actions/postsActions";

const PostForm = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        if(!user) {
            props.history.push('/');
        }
    }, [user, props.history])

    const [post, setPost] = useState({
        title: '',
        description: '',
        image: '',
    });

    const handleChange = (event) => {
        const {name, value} = event.target;
        setPost(prevState => ({
            ...prevState, [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setPost(prevSate => ({
            ...prevSate,
            [name]: file
        }));
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(post).forEach(key => {
            formData.append(key, post[key]);
        });
        dispatch(createPost(formData, user.token));
        props.history.push('/');
    }

    return (
        <form onSubmit={handleSubmit} className="post-form">
            <div className="form-row">
                <label>Title</label>
                <input name="title" onChange={handleChange} type="text"/>
            </div>
            <div className="form-row">
                <label>Description</label>
                <input name="description" onChange={handleChange} type="text"/>
            </div>
            <div className="form-row">
                <label>Image</label>
                <FileInput name="image" label="Image" onChange={fileChangeHandler}/>
            </div>
            <button>Create post</button>
        </form>
    );
};

export default PostForm;