import {Route, Switch} from 'react-router-dom';
import './App.css';
import Layout from "./components/Layout/Layout";
import Register from "./containers/Register/Register";
import LogIn from "./containers/Login/LogIn";
import PostList from "./containers/PostList/PostList";
import PostForm from "./containers/PostForm/PostForm";
import SinglePost from "./containers/PostList/SinglePost/SinglePost";
import CommentForm from "./containers/Comments/CommentForm/CommentForm";

const App = () => (
    <div className="App">
        <div className="container">
            <Layout>
                <Switch>
                    <Route path="/" component={PostList} exact/>
                    <Route path="/comments" component={PostList} exact/>
                    <Route path="/register" component={Register} exact/>
                    <Route path="/login" component={LogIn} exact/>
                    <Route path="/form" component={PostForm} exact/>
                    <Route path="/post/:id" component={SinglePost} exact/>
                    <Route path="/commentForm/:id" component={CommentForm} exact/>
                </Switch>
            </Layout>
        </div>
    </div>
);

export default App;
