const express = require('express');

////////////////////////////////////
const Comment = require("../models/Comment");
const auth = require("../myMiddleWare/auth");


const router = express.Router();

router.post('/', auth, async (req, res) => {
    try {

        const commentData = req.body;

        if (commentData.text.length === 0) {
            res.status(400).send({error: 'text should be filled'});
        }
        commentData.user = req.user;
        const comment = new Comment(commentData);
        await comment.save();
        res.send(comment);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {

    try {
        const criteria = {};
        if (req.query.post) {
            criteria.post = req.query.post;
        }
        const commentData = await Comment.find(criteria);
        res.send(commentData);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const commentData = await Comment.findOne({_id: req.params.id});
        if (commentData) {
            res.send(commentData);
        }
        res.sendStatus(404);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;


