const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const path = require("path");
////////////////////////////////////
const Post = require("../models/Post");
///////////////////////////////////
const config = require('../config');
const auth = require("../myMiddleWare/auth");


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
})

const upload = multer({storage});

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const postData = req.body;

        if(req.file) {
            postData.image = 'uploads/' + req.file.filename;
        }

        if (postData.description.length === 0 && !postData.image) {
            res.status(400).send({error: 'description or image should be filled'});
        }
        postData.user = req.user;
        postData.date = Date();
        const post = new Post(postData);
        await post.save();
        res.send(post);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {
    try {
        const postData = await Post.find().populate('user', 'username').sort({date: -1});
        res.send(postData);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const postData = await Post.findOne({_id: req.params.id}).populate('user', 'username');
        if (postData) {
            res.send(postData);
        }
        res.sendStatus(404);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;


