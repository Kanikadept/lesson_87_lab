const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true,
    },
    image: String,
    description: String,
    date: Date
});

const Post = mongoose.model('Post', PostSchema);
module.exports = Post;